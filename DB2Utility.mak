# Microsoft Developer Studio Generated NMAKE File, Based on DB2Utility.dsp
!IF "$(CFG)" == ""
CFG=DB2Utility - Win32 Debug
!MESSAGE No configuration specified. Defaulting to DB2Utility - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "DB2Utility - Win32 Release" && "$(CFG)" != "DB2Utility - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DB2Utility.mak" CFG="DB2Utility - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DB2Utility - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "DB2Utility - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DB2Utility - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\DB2Utility.exe"


CLEAN :
	-@erase "$(INTDIR)\DB2Utility.obj"
	-@erase "$(INTDIR)\DB2Utility.pch"
	-@erase "$(INTDIR)\DB2Utility.res"
	-@erase "$(INTDIR)\DB2UtilityDlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\DB2Utility.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\DB2Utility.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\DB2Utility.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\DB2Utility.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\DB2Utility.pdb" /machine:I386 /out:"$(OUTDIR)\DB2Utility.exe" 
LINK32_OBJS= \
	"$(INTDIR)\DB2Utility.obj" \
	"$(INTDIR)\DB2UtilityDlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\DB2Utility.res"

"$(OUTDIR)\DB2Utility.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "DB2Utility - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\DB2Utility.exe"


CLEAN :
	-@erase "$(INTDIR)\DB2Utility.obj"
	-@erase "$(INTDIR)\DB2Utility.pch"
	-@erase "$(INTDIR)\DB2Utility.res"
	-@erase "$(INTDIR)\DB2UtilityDlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\DB2Utility.exe"
	-@erase "$(OUTDIR)\DB2Utility.ilk"
	-@erase "$(OUTDIR)\DB2Utility.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)\DB2Utility.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\DB2Utility.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\DB2Utility.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\DB2Utility.pdb" /debug /machine:I386 /out:"$(OUTDIR)\DB2Utility.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\DB2Utility.obj" \
	"$(INTDIR)\DB2UtilityDlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\DB2Utility.res"

"$(OUTDIR)\DB2Utility.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("DB2Utility.dep")
!INCLUDE "DB2Utility.dep"
!ELSE 
!MESSAGE Warning: cannot find "DB2Utility.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "DB2Utility - Win32 Release" || "$(CFG)" == "DB2Utility - Win32 Debug"
SOURCE=.\DB2Utility.cpp

"$(INTDIR)\DB2Utility.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\DB2Utility.pch"


SOURCE=.\DB2Utility.rc

"$(INTDIR)\DB2Utility.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\DB2UtilityDlg.cpp

"$(INTDIR)\DB2UtilityDlg.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\DB2Utility.pch"


SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "DB2Utility - Win32 Release"

CPP_SWITCHES=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\DB2Utility.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\DB2Utility.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "DB2Utility - Win32 Debug"

CPP_SWITCHES=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)\DB2Utility.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\DB2Utility.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 


!ENDIF 

