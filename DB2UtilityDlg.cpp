// DB2UtilityDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DB2Utility.h"
#include "DB2UtilityDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDB2UtilityDlg dialog

CDB2UtilityDlg::CDB2UtilityDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDB2UtilityDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDB2UtilityDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDB2UtilityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDB2UtilityDlg)
	DDX_Control(pDX, IDC_USERNAME, m_Username);
	DDX_Control(pDX, IDC_PASSWORD, m_Password);
	DDX_Control(pDX, IDC_BEGIN, m_btnBegin);
	DDX_Control(pDX, IDC_SUFFIX, m_edtSuffix);
	DDX_Control(pDX, IDC_FILE, m_edtFileName);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDB2UtilityDlg, CDialog)
	//{{AFX_MSG_MAP(CDB2UtilityDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_QUIT, OnQuitClick)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowseClick)
	ON_BN_CLICKED(IDC_DETAIL_STRUCTURE, OnDetailStructure)
	ON_BN_CLICKED(IDC_RADIO_DEL, OnRadioDelSelected)
	ON_BN_CLICKED(IDC_RADIO_IXF, OnRadioIxfSelected)
	ON_BN_CLICKED(IDC_RADIO_WSF, OnRadioWsfSelected)
	ON_EN_CHANGE(IDC_FILE, OnChangeFileName)
	ON_BN_CLICKED(IDC_BEGIN, OnBeginClick)
	ON_BN_CLICKED(IDC_RADIO_CURRENT, OnRadioCurrent)
	ON_BN_CLICKED(IDC_RADIO_CUSTOM, OnRadioCustom)
	ON_BN_CLICKED(IDC_DETAIL_OPTION, OnDetailOption)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDB2UtilityDlg message handlers

BOOL CDB2UtilityDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	CheckDlgButton(IDC_RADIO_DEL,1);
	CheckDlgButton(IDC_RADIO_CURRENT,1);
	OnRadioCurrent();
	m_OutType = 0;
	m_edtSuffix.SetWindowText("DEL");
	m_btnBegin.EnableWindow(FALSE);
	/*
	m_btnCurrent = (CButton*)this->GetDlgItem(IDC_RADIO_CURRENT);
	m_btnCurrent->SetParent(GetDlgItem(IDC_STATIC_CONNECT));
	m_btnCustom = (CButton*)this->GetDlgItem(IDC_RADIO_CUSTOM);
	m_btnCustom->SetParent(GetDlgItem(IDC_STATIC_CONNECT));
	*/
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDB2UtilityDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDB2UtilityDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDB2UtilityDlg::OnQuitClick() 
{
	CDialog::OnCancel();	
}

void CDB2UtilityDlg::OnBrowseClick() 
{
	
	char szFilter[] = "SQL 文件 (*.sql)|*.sql|所有文件 (*.*)|*.*||";
	
	CFileDialog *cfd = new CFileDialog(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		szFilter,this);
	if(cfd->DoModal()==IDOK)
	{
		m_edtFileName.SetWindowText(cfd->GetPathName());
	}
	delete cfd;
	
	/*
	OPENFILENAME ofn;  // older version
	memset(&ofn, 0, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.lpstrFilter = szFilter;
	ofn.hwndOwner = m_hWnd;
	int nResult = ::GetOpenFileName(&ofn);
	m_edtFileName.SetWindowText(ofn.lpstrFile);
	*/
}

void CDB2UtilityDlg::OnDetailStructure() 
{	
	AfxMessageBox(IDS_STRUCTURE,MB_ICONINFORMATION|MB_OK);
}

void CDB2UtilityDlg::OnRadioDelSelected() 
{
	// TODO: Add your control notification handler code here
	m_edtSuffix.SetWindowText("DEL");
	m_OutType = 0;
}

void CDB2UtilityDlg::OnRadioIxfSelected() 
{
	// TODO: Add your control notification handler code here	
	m_edtSuffix.SetWindowText("IXF");
	m_OutType = 2;
}

void CDB2UtilityDlg::OnRadioWsfSelected() 
{
	// TODO: Add your control notification handler code here
	m_edtSuffix.SetWindowText("WSF");	
	m_OutType = 1;
}

void CDB2UtilityDlg::OnChangeFileName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	char szFileName[MAX_PATH];
	m_edtFileName.GetWindowText(szFileName,MAX_PATH);
	CString FileName = szFileName;
	FileName.TrimRight();
	if(FileName.GetLength()==0)
		m_btnBegin.EnableWindow(FALSE);
	else
		m_btnBegin.EnableWindow(TRUE);
}

void CDB2UtilityDlg::OnBeginClick() 
{
	char szFileExt[MAX_PATH];
	//判断文件名是否为空
	m_edtFileName.GetWindowText(szFileExt,MAX_PATH);
	m_FileName = szFileExt;
	m_FileName.TrimRight();
	if(m_FileName.GetLength()==0)
	{
		MessageBox("请先选择数据库结构文件，再点击开始。",NULL,MB_ICONERROR|MB_OK);
		return;
	}
	//读取文件所在的目录
	int idx = m_FileName.ReverseFind('\\');
	if(idx!=-1)
		m_FilePath = m_FileName.Left(idx+1);
	else		
		m_FilePath.Empty();
	//判断输出文件的扩展名是否为空，如果为空，根据选择的类型设置缺省的扩张名
	m_edtSuffix.GetWindowText(szFileExt,MAX_PATH);
	m_FileExt = szFileExt;
	m_FileExt.TrimRight();
	if(m_FileExt.GetLength()==0)
	{
		switch(m_OutType){
		case 0:
			m_FileExt = "DEL";
			break;
		case 1:
			m_FileExt = "WSF";
			break;
		case 2:
			m_FileExt = "IXF";
		}
	}
	CStdioFile readFile;
	CFileException ex;
	CStringList TableList(50);	//存放数据表的列表

	if (!readFile.Open(m_FileName, CFile::modeRead, &ex))
	{
		TCHAR   szCause[1024];
		CString strFormatted;

		ex.GetErrorMessage(szCause, 1024);
		strFormatted = _T("打开数据库结构文件失败！\n错误信息：");
		strFormatted += szCause;
		MessageBox(strFormatted,NULL,MB_ICONERROR|MB_OK);
	}
	else
	{
		//读取数据库中的所有表并保存这些表名
		CString csLine;
		CString csTable;
		int iBegin=-1,iEnd=-1;
		//解析数据库名称，和所有表名
		while(readFile.ReadString(csLine)){
			if(csLine.Find("CONNECT TO")!=-1){
				//在此可以读取数据库名
				iBegin = csLine.Find("TO ")+3;
				iEnd = csLine.Find(';');
				m_Database = csLine.Mid(iBegin,iEnd-iBegin);
			}
			if(csLine.Find("CREATE TABLE")==-1)
				continue;
			//处理该字串得到表名
			iBegin = csLine.Find("\".\"")+3;
			iEnd = csLine.Find("\"  (");
			csTable = csLine.Mid(iBegin,iEnd-iBegin);
			//MessageBox(csTable,NULL,MB_OK);
			TableList.AddTail(csTable);
		}
		readFile.Close();
		//判断是否需要连接到数据库
		bool ConnectIt = IsDlgButtonChecked(IDC_CONNECT)!=0;
		bool UseCurrent= IsDlgButtonChecked(IDC_RADIO_CURRENT)!=0;
		CString usr;
		CString pwd;
		if(ConnectIt)
		{
			//都用户名和口令
			if(!UseCurrent){
				GetDlgItem(IDC_USERNAME)->GetWindowText(usr);
				GetDlgItem(IDC_PASSWORD)->GetWindowText(pwd);
			}
		}
		//写输出文件
		CStdioFile load_file;
		CStdioFile unload_file;
		CString csload;
		bool bload = false;
		CString csunload;
		bool bunload = false;
		csload.Format("%s%s",m_FilePath,LOAD_FILE_NAME);
		csunload.Format("%s%s",m_FilePath,UNLOAD_FILE_NAME);
		if (!load_file.Open(csload, CFile::modeCreate|CFile::modeWrite,&ex))
		{
			TCHAR   szCause[1024];
			CString strFormatted;

			ex.GetErrorMessage(szCause, 1024);
			strFormatted = _T("打开数据加载文件失败！\n错误信息：");
			strFormatted += szCause;
			MessageBox(strFormatted,NULL,MB_ICONERROR|MB_OK);
			return;
		}
		else
			bload = true;
		if (!unload_file.Open(csunload, CFile::modeCreate|CFile::modeWrite,&ex))
		{
			TCHAR   szCause[1024];
			CString strFormatted;

			ex.GetErrorMessage(szCause, 1024);
			strFormatted = _T("打开数据卸载文件失败！\n错误信息：");
			strFormatted += szCause;
			MessageBox(strFormatted,NULL,MB_ICONERROR|MB_OK);
			return;
		}
		else
			bunload = true;
		//写文件		
		CString csOutLine;
		CString csTableName;
		CString csStype;
		switch(m_OutType){
		case 0:
			csStype = "DEL";
			break;
		case 1:
			csStype = "WSF";
			break;
		case 2:
			csStype = "IXF";
			break;
		default:
			csStype = "TXT";
		}
		m_FileExt.MakeLower();
		//写连接语句
		if(ConnectIt)
		{
			if(UseCurrent){
				csOutLine.Format("DB2 CONNECT TO %s\n\n",m_Database);
			}
			else{
				CString usr,pwd;
				GetDlgItem(IDC_USERNAME)->GetWindowText(usr);
				GetDlgItem(IDC_PASSWORD)->GetWindowText(pwd);
				csOutLine.Format("DB2 CONNECT TO %s USER %s USING %s\n\n",m_Database,usr,pwd);
			}
			unload_file.WriteString(csOutLine);
			load_file.WriteString(csOutLine);
		}
		for(POSITION pos = TableList.GetHeadPosition(); pos != NULL;)
		{
			csTableName = TableList.GetNext(pos);
			csTableName.MakeLower();
			csOutLine.Format("DB2 EXPORT TO %s.%s OF %s SELECT * FROM %s\n",
				csTableName,m_FileExt,csStype,csTableName);
			unload_file.WriteString(csOutLine);
			csOutLine.Format("DB2 IMPORT FROM %s.%s OF %s INSERT INTO %s\n",
				csTableName,m_FileExt,csStype,csTableName);
			load_file.WriteString(csOutLine);
		}
		//写断开连接语句
		if(ConnectIt)
		{
			unload_file.WriteString("\nDB2 CONNECT RESET");
			load_file.WriteString("\nDB2 CONNECT RESET");
		}
		if(bload)
			load_file.Close();
		if(bunload)
			unload_file.Close();
		//显示成功对话框
		if(AfxMessageBox("操作已经成功结束，是否查看输出结果？",
			MB_ICONQUESTION|MB_YESNO)==IDYES)
		{
			ShellExecute(m_hWnd,"open","notepad",csload,NULL,SW_SHOW);
			ShellExecute(m_hWnd,"open","notepad",csunload,NULL,SW_SHOW);
		}
	}
}

void CDB2UtilityDlg::OnRadioCurrent() 
{
	m_Password.EnableWindow(FALSE);
	m_Username.EnableWindow(FALSE);
}

void CDB2UtilityDlg::OnRadioCustom() 
{
	m_Password.EnableWindow(TRUE);
	m_Username.EnableWindow(TRUE);
	m_Username.SetFocus();
}

void CDB2UtilityDlg::OnDetailOption() 
{
	AfxMessageBox(IDS_OPTION,MB_OK|MB_ICONINFORMATION);
}
