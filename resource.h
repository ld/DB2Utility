//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DB2Utility.rc
//
#define IDS_STRUCTURE                   1
#define IDS_OPTION                      2
#define IDD_DB2UTILITY_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_FILE                        1000
#define IDC_BROWSE                      1001
#define IDC_DETAIL_STRUCTURE            1002
#define IDC_QUIT                        1003
#define IDC_BEGIN                       1004
#define IDC_DETAIL_OPTION               1005
#define IDC_RADIO_DEL                   1006
#define IDC_RADIO_IXF                   1007
#define IDC_RADIO_WSF                   1008
#define IDC_SUFFIX                      1009
#define IDC_RADIO_CURRENT               1014
#define IDC_RADIO_CUSTOM                1015
#define IDC_USERNAME                    1016
#define IDC_PASSWORD                    1017
#define IDC_CONNECT                     1019
#define IDC_STATIC_CONNECT              1020
#define IDC_STATIC_OUTPUT               1021
#define IDC_COMBO1                      1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
