// DB2UtilityDlg.h : header file
//

#if !defined(AFX_DB2UTILITYDLG_H__68593485_4A8F_4735_862C_5756DAAA197B__INCLUDED_)
#define AFX_DB2UTILITYDLG_H__68593485_4A8F_4735_862C_5756DAAA197B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LOAD_FILE_NAME "load.bat"
#define UNLOAD_FILE_NAME "unload.bat"

/////////////////////////////////////////////////////////////////////////////
// CDB2UtilityDlg dialog

class CDB2UtilityDlg : public CDialog
{
private:
	int m_OutType;		//0: ASCII ; 1: 工作表格式  ; 2: 集成交换格式
	CString m_FileExt;	//输出数据文件的扩展名
	CString m_FileName;	//输入文件的全路径包括文件名
	CString m_FilePath;	//产生文件放置的目录（与数据库结构文件在同一个目录）
	CString m_Database; //数据库名称
// Construction
public:
	CDB2UtilityDlg(CWnd* pParent = NULL);	// standard constructor
	
// Dialog Data
	//{{AFX_DATA(CDB2UtilityDlg)
	enum { IDD = IDD_DB2UTILITY_DIALOG };
	CEdit	m_Username;
	CEdit	m_Password;
	CButton	m_btnBegin;
	CEdit	m_edtSuffix;
	CEdit	m_edtFileName;
	CButton* m_btnCurrent;
	CButton* m_btnCustom;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDB2UtilityDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDB2UtilityDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnQuitClick();
	afx_msg void OnBrowseClick();
	afx_msg void OnDetailStructure();
	afx_msg void OnRadioDelSelected();
	afx_msg void OnRadioIxfSelected();
	afx_msg void OnRadioWsfSelected();
	afx_msg void OnChangeFileName();
	afx_msg void OnBeginClick();
	afx_msg void OnRadioCurrent();
	afx_msg void OnRadioCustom();
	afx_msg void OnDetailOption();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DB2UTILITYDLG_H__68593485_4A8F_4735_862C_5756DAAA197B__INCLUDED_)
