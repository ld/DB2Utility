; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDB2UtilityDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DB2Utility.h"

ClassCount=2
Class1=CDB2UtilityApp
Class2=CDB2UtilityDlg

ResourceCount=3
Resource2=IDR_MAINFRAME
Resource3=IDD_DB2UTILITY_DIALOG

[CLS:CDB2UtilityApp]
Type=0
HeaderFile=DB2Utility.h
ImplementationFile=DB2Utility.cpp
Filter=N

[CLS:CDB2UtilityDlg]
Type=0
HeaderFile=DB2UtilityDlg.h
ImplementationFile=DB2UtilityDlg.cpp
Filter=D
LastObject=IDC_CONNECT
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_DB2UTILITY_DIALOG]
Type=1
Class=CDB2UtilityDlg
ControlCount=22
Control1=IDC_FILE,edit,1350631552
Control2=IDC_BROWSE,button,1342242817
Control3=IDC_DETAIL_STRUCTURE,button,1342242816
Control4=IDC_RADIO_DEL,button,1342406665
Control5=IDC_RADIO_WSF,button,1342275593
Control6=IDC_RADIO_IXF,button,1342275593
Control7=IDC_SUFFIX,edit,1350631552
Control8=IDC_CONNECT,button,1342242819
Control9=IDC_RADIO_CURRENT,button,1342341129
Control10=IDC_RADIO_CUSTOM,button,1342275593
Control11=IDC_USERNAME,edit,1350631552
Control12=IDC_PASSWORD,edit,1350631584
Control13=IDC_BEGIN,button,1342242816
Control14=IDC_DETAIL_OPTION,button,1342242816
Control15=IDC_QUIT,button,1342242816
Control16=IDC_STATIC_OUTPUT,button,1342177287
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,static,1342308352
Control20=IDC_STATIC_CONNECT,button,1342177287
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352

